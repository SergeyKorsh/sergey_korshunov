package com.trainin.korshunov;

public class SortingContext {
    Sorter strategy;

    public SortingContext(Sorter strategy) {
        this.strategy = strategy;
    }

    public void sort(int[] mas) {
        strategy.sort(mas);
    }
}
