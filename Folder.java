package com.trainin.korshunov;

import java.util.ArrayList;
import java.util.List;

public class Folder {
    private String folderName;
    List<File> files = new ArrayList<File>();
    List<Folder> folders = new ArrayList<Folder>();

    Folder(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderName() {
        return this.folderName;
    }

    //Добавление каталогов
    public void add(Folder folder) {
        boolean flag = true;
        for (Folder value : folders) {
            if (folder.folderName.equals(value.folderName)) {
                flag = false;
                break;
            }
        }
        if (flag) {
            folders.add(folder);
        }
    }

    //Добавление файлов
    public void add(File file) {
        boolean flag = true;
        for (File value : files) {
            if (file.getFileName().equals(value.getFileName())) {
                flag = false;
                break;
            }
        }
        if (flag) {
            files.add(file);
        }
    }

    //Вывод каталогов и файлов
    public void print(int layer) {
        for (File file : files) {
            System.out.println(tab(layer) + file.getFileName());
        }
        for (int i = 0; i < folders.size(); i++) {
            System.out.print(tab(layer) + folders.get(i).folderName);
            if (folders.get(i).folders.size() != 0 || folders.get(i).files.size() != 0) {
                System.out.print("\\\n");
//                }
                folders.get(i).print(layer + 1);
            } else System.out.print("\n");
        }
    }

    //Отступ для вывода
    public String tab(int i) {
        String tab = "";
        for (int j = 0; j < i; j++) {
            tab += "\t";
        }
        return tab;
    }

    //Добавление по введенному пути
    public void addPath(String[] path) {
        Folder tmpFolder = this;
        for (String folderOrFile : path) {
            if (!folderOrFile.substring(folderOrFile.length() - 4).equals(".txt")) {
                int check = checkDuplicateFolder(folderOrFile, tmpFolder);
                if (check != -1) {
                    tmpFolder = tmpFolder.folders.get(check);
                } else {
                    tmpFolder.add(new Folder(folderOrFile));
                    tmpFolder = tmpFolder.folders.get(tmpFolder.folders.size() - 1);
                }
            } else {
                if (!checkDuplicateFile(folderOrFile, tmpFolder)) {
                    tmpFolder.files.add(new File(folderOrFile));
                }
                break;
            }
        }
    }

    //Проверка на совпадение каталогов
    public int checkDuplicateFolder(String folderName, Folder folder) {
        for (int i = 0; i < folder.folders.size(); i++) {
            if (folderName.equals(folder.folders.get(i).folderName)) {
                return i;
            }
        }
        return -1;
    }

    //Проверка на совпадение файлов
    public boolean checkDuplicateFile(String fileName, Folder folder) {
        for (int i = 0; i < folder.files.size(); i++) {
            if (fileName.equals(folder.files.get(i).getFileName())) {
                return true;
            }
        }
        return false;
    }
}
