package com.trainin.korshunov;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Folder folder = new Folder("ROOT");
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        System.out.println("Команды для ввода:");
        System.out.println("1) вывод структуры - print;");
        System.out.println("2) выход из программы - exit;");
        System.out.println("Вводить пути через знак </>, также вводить только на латинскими буквами");
        do{
            System.out.println("Ввод команды:");
            String input = scanner.nextLine();
            if(input.equals("exit")){
                flag=false;
            }
            else if(input.equals("print")){
                System.out.println(folder.getFolderName()+"\\");
                folder.print(1);
            }
            else {
                String[] path = input.split("/");
                folder.addPath(path);
            }
        }
        while (flag);
    }
}
