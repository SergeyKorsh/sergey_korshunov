package com.trainin.korshunov;

public class SelectionSort implements Sorter {
    public void sort(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            int min = mas[i];
            int min_i = i;
            for (int j = i + 1; j < mas.length; j++) {
                if (mas[j] < min) {
                    min = mas[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = mas[i];
                mas[i] = mas[min_i];
                mas[min_i] = tmp;
            }
        }
    }
}
