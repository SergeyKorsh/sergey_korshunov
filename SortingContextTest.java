package com.trainin.korshunov;

import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.*;

class SortingContextTest {

    @org.junit.jupiter.api.Test
    void sort() {
        int[] mas = {1, 5, 4, 6, 2};
        SortingContext context = new SortingContext(new SelectionSort());
        context.sort(mas);
        for (int i=0; i<mas.length;i++) {
            System.out.printf(""+ mas[i]+"\n");
        }
        assertEquals(2, mas[1]);
        mas[4] = 0;
        context = new SortingContext(new BubbleSort());
        context.sort(mas);
        for (int i=0; i<mas.length+1;i++) {
            System.out.printf(""+ mas[i]);
        }
        assertEquals(0, mas[0]);


    }
}